from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
import stripe

from payment.models import Payment
from siteisrunning.settings import STRIPE_WEBHOOK_ENDPOINT_SECRET, STRIPE_API_KEY, STRIPE_PLAN_PRO_SKU


@login_required
def canceled_checkout_account(request):
    return render(request, 'canceled_checkout_account.html', {})


@login_required
def success_checkout_account(request):
    return render(request, 'success_checkout_account.html', {})


@login_required
def checkout_account(request):
    user = request.user
    if user:
        return render(request, 'checkout_account_form.html',
                      {'user': user, 'STRIPE_API_KEY': STRIPE_API_KEY, 'STRIPE_PLAN_PRO_SKU': STRIPE_PLAN_PRO_SKU})
    else:
        raise HttpResponse(status=500)


@csrf_exempt
def checkout_account_webhook_endpoint(request):
    endpoint_secret = STRIPE_WEBHOOK_ENDPOINT_SECRET
    event = None

    payload = request.body
    sig_header = request.META['HTTP_STRIPE_SIGNATURE']

    try:
        event = stripe.Webhook.construct_event(
            payload, sig_header, endpoint_secret
        )
    except ValueError as e:
        # Invalid payload
        return HttpResponse(status=400)
    except stripe.error.SignatureVerificationError as e:
        # Invalid signature
        return HttpResponse(status=500)

    # Handle the checkout.session.completed event
    if event['type'] == 'checkout.session.completed':
        session = event['data']['object']

        # Fulfill the purchase
        if fulfill_checkout(session):
            return HttpResponse(status=200)
        else:
            return HttpResponse(messages=500)


def fulfill_checkout(session):
    if session.client_reference_id:
        user_id = int(session.client_reference_id)
        if Payment.objects.filter(owner_id=user_id).exists():
            payment = Payment.objects.get(owner_id=user_id)
        else:
            payment = Payment.objects.create(owner_id=user_id)

        if payment.add_user_paying():
            return True
        else:
            return False

    return False

from django.db import models
import datetime
from django.utils import timezone

from check.models import Check
from siteisrunning.settings import BASIC_PLAN_MAX_ACTIVE_CHECKS
from users.models import CustomUser


class Payment(models.Model):
    owner = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    payment_expires_at = models.DateTimeField(null=True, blank=True)
    expires_reminder_sent_at = models.DateTimeField(null=True, blank=True)

    def is_user_paying_expired(self):
        if not self.payment_expires_at:
            return True
        if self.payment_expires_at + datetime.timedelta(days=1) < timezone.now():
            return True
        if self.payment_expires_at + datetime.timedelta(days=1) > timezone.now():
            return False

    @staticmethod
    def is_user_paying(user):
        if Payment.objects.filter(owner_id=user.id).exists():
            payment = Payment.objects.get(owner_id=user.id)
            if not payment.is_user_paying_expired():
                return True
            else:
                return False
        else:
            return False

    def is_user_paying_expiring_soon(self, weeks_before=2):
        if self.payment_expires_at:
            if self.payment_expires_at - datetime.timedelta(weeks=weeks_before) < timezone.now():
                return True
            if self.payment_expires_at - datetime.timedelta(weeks=weeks_before) > timezone.now():
                return False

    def expires_user_paying(self, secure=True):
        if secure and not self.is_user_paying_expired():
            return False

        try:
            self.delete()
            return True
        except:
            return False

    def add_user_paying(self):
        if self.payment_expires_at:
            self.payment_expires_at = self.payment_expires_at + datetime.timedelta(days=365)
            self.expires_reminder_sent_at = None
        else:
            self.payment_expires_at = timezone.now() + datetime.timedelta(days=365)
            self.expires_reminder_sent_at = None

        try:
            self.save()
            return True
        except:
            return False

    @staticmethod
    def allowed_add_more_checks(user):
        checks = Check.objects.filter(owner_id=user.id, active=True)
        if Payment.objects.filter(owner_id=user.id).exists():
            payment = Payment.objects.get(owner_id=user.id)
            if not payment.is_user_paying_expired():
                return True
            else:
                return False
        elif checks.count() > BASIC_PLAN_MAX_ACTIVE_CHECKS:
            return False
        else:
            return True

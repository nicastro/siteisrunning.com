from django.conf.urls import url

from check.views import debug
from .views import checkout_account, checkout_account_webhook_endpoint, \
    success_checkout_account, canceled_checkout_account

urlpatterns = [
    url(r"^checkout/$", checkout_account, name='checkout_account'),
    url(r"^checkout_endpoint/$", checkout_account_webhook_endpoint, name='checkout_account_webhook_endpoint'),
    url(r"^checkout/success/$", success_checkout_account, name='success_checkout_account'),
    url(r"^checkout/canceled/$", canceled_checkout_account, name='canceled_checkout_account'),
    url('debug', debug, name='debug'),

]

import datetime

from allauth.account.models import EmailAddress
from django.template.loader import render_to_string
from django.utils import timezone
from django_common.auth_backends import User
from django_cron import CronJobBase, Schedule
from django.core.mail import send_mass_mail

from check.models import Check
from payment.models import Payment
from siteisrunning.settings import DEFAULT_FROM_EMAIL


class CronUserExpired(CronJobBase):
    RUN_EVERY_MINS = 0
    MIN_NUM_FAILURES = 5  # Before get notified

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'payment.cron_user_expired'  # a unique code

    def do(self):
        payments = Payment.objects.filter(payment_expires_at__isnull=False)
        for payment in payments:
            if payment.expires_user_paying(True):

                user = User.objects.get(id=payment.owner_id)
                mail_plain = render_to_string('email/subscription_expired.txt', {'user': user})
                emails = EmailAddress.objects.filter(user_id=user.id, verified=1)
                messages_to = []

                for user_email in emails:
                    msg = (
                        '[siteisrunning.com] Subscription is expired',
                        mail_plain,
                        DEFAULT_FROM_EMAIL,
                        [user_email.email]
                    )
                    messages_to.append(msg)

                send_mass_mail(messages_to, fail_silently=False)


class CronUserExpiring(CronJobBase):
    RUN_EVERY_MINS = 0
    MIN_NUM_FAILURES = 5  # Before get notified

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'payment.cron_user_expiring'  # a unique code

    def do(self):
        payments = Payment.objects.filter(payment_expires_at__isnull=False)
        for payment in payments:
            if payment.is_user_paying_expiring_soon(weeks_before=2):

                # Notify 2 weeks and 1 week before
                if not payment.expires_reminder_sent_at or payment.expires_reminder_sent_at < timezone.now() - datetime.timedelta(
                        days=7):

                    payment.expires_reminder_sent_at = timezone.now()
                    payment.save()
                    user = User.objects.get(id=payment.owner_id)
                    expiring_date = payment.payment_expires_at.strftime('%Y-%m-%d')
                    mail_plain = render_to_string('email/subscription_expires.txt',
                                                  {'user': user, 'expiring_date': expiring_date})

                    emails = EmailAddress.objects.filter(user_id=user.id, verified=1)
                    messages_to = []
                    for user_email in emails:
                        msg = (
                            '[siteisrunning.com] Subscription is expiring',
                            mail_plain,
                            DEFAULT_FROM_EMAIL,
                            [user_email.email]
                        )

                        messages_to.append(msg)

                    send_mass_mail(messages_to, fail_silently=False)


class CronUserExpiringDisableChecks(CronJobBase):
    RUN_EVERY_MINS = 0
    MIN_NUM_FAILURES = 5  # Before get notified

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'payment.cron_user_expiring_disable_check'  # a unique code

    def do(self):
        users = User.objects.all()
        for user in users:
            if not Payment.is_user_paying(user):
                checks = Check.objects.filter(owner_id=user.id)
                # Allow only 4 active
                # Offset is a bit weird with this ORM Query -> doing oldschool ;)
                count = 0
                for check in checks:
                    if check.active:
                        count += 1
                    if count > 4:
                        check.active = False
                        check.save()

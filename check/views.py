from django.utils import timezone
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, redirect
from check.forms import CheckForm
from check.models import Check
from payment.models import Payment
from siteisrunning.settings import BASIC_PLAN_MIN_PERIOD_CHECK_MINUTES, BASIC_PLAN_MIN_WHEN_OFFLINE_SEND_NEXT_MINUTES
import datetime


def debug(request):
    a = 1


@login_required
def show_check(request):
    user = request.user
    checks = Check.objects.filter(owner=request.user)
    disabled_html_class = 'disabled'
    if Payment.allowed_add_more_checks(user):
        disabled_html_class = ''

    return render(request, 'show.html', {'checks': checks, 'user': user, 'disabled_html_class': disabled_html_class})


@login_required
def create_check(request):
    user = request.user
    if not Payment.allowed_add_more_checks(user):
        messages.error(request,
                       'You use Basic Plan. You can have max. 4 active Checks. Upgrade to Pro under "Account" Tab')
        return redirect(show_check)

    form = CheckForm(request.POST or None)

    if form.is_valid():
        form_data = form.save(commit=False)
        form_data.owner = request.user
        form_data.created_at = timezone.now()
        form_data.updated_at = timezone.now()
        form_data.checked_at = timezone.now() - datetime.timedelta(
            days=50)  # Ensure next check will be triggered at next cron

        check = form.save()

        if not Payment.is_user_paying(request.user):
            _basic_plan_downgrade(check, request)
        else:
            if not check.when_offline_period_send_mail == 0 and form_data.when_offline_period_send_mail < 30:
                check.when_offline_period_send_mail = 30
                messages.success(request, '"When offline, send next mail all" set to minimum 30 minutes.')
                check.save()

        if not Payment.allowed_add_more_checks(request.user):
            _basic_plan_ensure_max_checks(check, request)

        messages.success(request,
                         'New Website Check created successfully. Be aware: The first check will be in few minutes.')
        return redirect('show_check')

    return render(request, 'create_form.html', {'form': form, 'title_prefix': 'Create'})


@login_required
def update_check(request, id):
    check = Check.objects.get(id=id)
    if check.owner != request.user:
        raise PermissionDenied('Not Found')

    form = CheckForm(request.POST or None, instance=check)

    if form.is_valid():
        form_data = form.save(commit=False)
        form_data.updated_at = timezone.now()
        form_data.checked_at = timezone.now() - datetime.timedelta(
            days=50)  # Ensure next check will be triggered at next cron

        check = form.save()

        if not Payment.is_user_paying(request.user):
            _basic_plan_downgrade(check, request)
        else:
            if not check.when_offline_period_send_mail == 0 and form_data.when_offline_period_send_mail < 30:
                check.when_offline_period_send_mail = 30
                messages.success(request, '"When offline, send next mail all" set to minimum 30 minutes.')
                check.save()

        if not Payment.allowed_add_more_checks(request.user):
            _basic_plan_ensure_max_checks(check, request)

        messages.success(request, 'Website Check edited successfully. Be aware: The next check will be in few minutes.')
        return redirect('show_check')

    return render(request, 'create_form.html', {'form': form, 'check': check, 'title_prefix': 'Edit'})


def _basic_plan_downgrade(check, request):
    if check.period_check < BASIC_PLAN_MIN_PERIOD_CHECK_MINUTES:
        check.period_check = BASIC_PLAN_MIN_PERIOD_CHECK_MINUTES
        messages.error(request,
                       'You use Basic Plan. Your minimum period check is 4h. Upgrade to Pro under "Account" Tab')

    if not check.when_offline_period_send_mail == 0 and check.when_offline_period_send_mail < BASIC_PLAN_MIN_WHEN_OFFLINE_SEND_NEXT_MINUTES:
        check.when_offline_period_send_mail = BASIC_PLAN_MIN_WHEN_OFFLINE_SEND_NEXT_MINUTES
        messages.error(request,
                       'You use Basic Plan. Your minimum reminder period is 8h. Upgrade to Pro under "Account" Tab')

    check.save()


def _basic_plan_ensure_max_checks(check, request):
    if check.active:
        check.active = False
        messages.error(request,
                       'You use Basic Plan. You can have max. 4 active Checks. Upgrade to Pro under "Account" Tab')
        check.save()


@login_required
def delete_check(request, id):
    check = Check.objects.get(id=id)
    if check.owner != request.user:
        raise PermissionDenied('Not Found')

    if request.method == 'POST':
        check.delete()
        messages.success(request, 'Website Check deleted successfully')
        return redirect('show_check')

    return render(request, 'delete_confirm.html', {'check': check})

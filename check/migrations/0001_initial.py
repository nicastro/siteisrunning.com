# Generated by Django 2.2.3 on 2019-07-24 14:55

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Check',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('description', models.TextField(blank=True)),
                ('url', models.URLField(max_length=255, verbose_name='Website/Server')),
                ('active', models.BooleanField()),
                ('online', models.BooleanField(default=True)),
                ('period_check', models.PositiveIntegerField()),
                ('when_offline_period_send_mail', models.PositiveIntegerField()),
                ('offline_last_mail_sent', models.DateTimeField(blank=True, help_text='If offline and mail was sent. Empty if site is online.', null=True)),
                ('checked_at', models.DateTimeField()),
                ('created_at', models.DateTimeField()),
                ('updated_at', models.DateTimeField()),
            ],
        ),
    ]

Dear {{ user.username }}

The Site "{{check.name}}" has encountered a problem!

Website: {{check.url}}
HTTP Status: {{status}}
URL to check: http://siteisrunning.com/websites/update/{{check.id}}
{%if check.when_offline_period_send_mail > 0 %}
If the problem persists you will be informed again in {{check.when_offline_period_send_mail}} minutes.
{%else%}
You'll be not informed again until the problem is solved. You set "WHEN OFFLINE, SEND ALL" to 0.
{%endif%}Can be changed in the field: "WHEN OFFLINE, SEND ALL".

Best
siteisrunning.com

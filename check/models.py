import datetime

from django.db import models
from django.utils import timezone

from users.models import CustomUser


class Check(models.Model):
    owner = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    name = models.CharField('Name', max_length=255)
    description = models.TextField(blank=True)
    url = models.URLField('Website/Server', max_length=255)
    active = models.BooleanField()
    online = models.BooleanField(default=True)
    period_check = models.PositiveIntegerField()
    when_offline_period_send_mail = models.PositiveIntegerField()
    offline_last_mail_sent = models.DateTimeField(null=True,
                                                  blank=True,
                                                  help_text="If offline and mail was sent. Empty if site is online.")
    checked_at = models.DateTimeField()
    created_at = models.DateTimeField()  # Auto add is bad
    updated_at = models.DateTimeField()  # Auto add is bad

    @staticmethod
    def is_down(status):
        if not status:
            return True
        elif str(status).startswith('4') or str(status).startswith('5'):
            return True
        elif status == 'SSLCert':
            return True
        elif status == 'ConnectionRefused':
            return True
        elif status == 'TimeoutError':
            return True
        elif status == 'ConnectionAbortedError':
            return True
        elif status == 'ConnectionAborted':
            return True
        elif status == 'ConnectionResetError':
            return True
        elif status == 'ConnectionError':
            return True

        return False

    def time_to_check(self):
        next_check = self.checked_at + datetime.timedelta(minutes=self.period_check)

        # Time passed?
        if next_check <= timezone.now():
            return True
        else:
            return False

    def time_to_send_mail(self):
        # If not set, this means its the first email which will be sent
        if not self.offline_last_mail_sent:
            return True

        next_mail = self.offline_last_mail_sent + datetime.timedelta(minutes=self.when_offline_period_send_mail)

        # Time passed?
        if next_mail <= timezone.now():
            # If set 0, user doesn't want more emails
            if self.when_offline_period_send_mail == 0:
                return False

            return True
        else:
            return False

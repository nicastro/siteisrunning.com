from django.urls import path
from .views import show_check, create_check, update_check, delete_check, debug

urlpatterns = [
    path('overview', show_check, name='show_check'),
    path('new', create_check, name='create_check'),
    path('update/<int:id>/', update_check, name='update_check'),
    path('delete/<int:id>/', delete_check, name='delete_check'),

    path('debug', debug, name='debug'),

]

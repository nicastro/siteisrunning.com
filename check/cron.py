from _ssl import SSLCertVerificationError

import httplib2
import validators
from allauth.account.models import EmailAddress
from django.template.loader import render_to_string
from django.utils import timezone
from django_common.auth_backends import User
from django_cron import CronJobBase, Schedule
from django.core.mail import send_mass_mail

from check.models import Check


class CronCheckRunning(CronJobBase):
    # pdb.set_trace()
    RUN_EVERY_MINS = 0  # every 2 hours
    MIN_NUM_FAILURES = 5  # Before get notified

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'check.cron_check_running'  # a unique code

    def do(self):
        # Check Status
        checks = Check.objects.filter(active=True)
        for check in checks:

            if check.time_to_check():
                if validators.url(check.url):
                    status = self.check_url(check.url)

                    if check.is_down(status):
                        check.online = False
                        if check.time_to_send_mail():  # Send Email
                            if self.alert_user(check, status):
                                check.offline_last_mail_sent = timezone.now()
                    else:
                        check.online = True
                        check.offline_last_mail_sent = None

                    check.checked_at = timezone.now()
                    check.save()

    @staticmethod
    def alert_user(check, status):
        user = User.objects.get(id=check.owner_id)
        emails = EmailAddress.objects.filter(user_id=check.owner_id, verified=1)

        mail_plain = render_to_string('email/email.txt', {'check': check, 'status': status, 'user': user})
        messages_to = []
        for user_email in emails:
            msg = (
                '[siteisrunning.com] Problem with: ' + check.name,
                mail_plain,
                'noreply@siteisrunning.com',
                [user_email.email]
            )

            messages_to.append(msg)

        result = send_mass_mail(messages_to, fail_silently=False)

        # result returns sent mails. We don't want control if all mail are sent.
        # If sent min. 1 mail, than we return true
        if result >= 1:
            return True

        return False

    @staticmethod
    def check_url(url):
        try:
            http2 = httplib2.Http()
            # http2.follow_all_redirects = True
            http2.follow_redirects = True

            resp, content = http2.request(url,
                                          method="GET",
                                          body=None,
                                          headers={"User-agent": "Mozilla/5.0"},
                                          redirections=httplib2.DEFAULT_MAX_REDIRECTS,
                                          connection_type=None, )
            return resp.status

        except httplib2.ServerNotFoundError:
            return False
        except SSLCertVerificationError:
            return 'SSLCert'
        except TimeoutError:
            return 'TimeoutError'
        except ConnectionRefusedError:
            return 'ConnectionRefused'
        except ConnectionAbortedError:
            return 'ConnectionAborted'
        except ConnectionResetError:
            return 'ConnectionResetError'
        except ConnectionError:
            return 'ConnectionError'

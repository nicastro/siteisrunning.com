from django import forms
from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils import timezone

from .models import Check


class CheckForm(forms.ModelForm):
    active = forms.BooleanField(initial=1, required=0)
    name = forms.CharField(required=1, widget=forms.Textarea(attrs={'placeholder': 'MyName/Customer name'}))
    description = forms.CharField(required=0, widget=forms.TextInput(attrs={'placeholder': 'Optional description'}))
    url = forms.URLField(required=1, widget=forms.URLInput(attrs={'placeholder': 'http:// or https://'}))

    period_check = forms.IntegerField(min_value=5, max_value=43200, initial=60)
    when_offline_period_send_mail = forms.IntegerField(min_value=0, max_value=43200, initial=0)

    checked_at = forms.DateTimeField(initial=timezone.now(), disabled=1)
    created_at = forms.DateTimeField(initial=timezone.now(), disabled=1)
    updated_at = forms.DateTimeField(initial=timezone.now(), disabled=1)

    class Meta:
        model = Check
        fields = ['name', 'url', 'description', 'active', 'period_check', 'when_offline_period_send_mail',
                  'checked_at', 'created_at', 'updated_at']

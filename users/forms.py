import pytz
from captcha.fields import ReCaptchaField
from captcha.widgets import ReCaptchaV2Checkbox
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import CustomUser
from django import forms

from django.contrib.auth import get_user_model

User = get_user_model()


class CustomUserCreationForm(UserCreationForm):
    company = forms.CharField(required=False)
    accept_term = forms.BooleanField(required=True)
    timezone = forms.ChoiceField(required=False, choices=[(t, t) for t in pytz.common_timezones])
    captcha = ReCaptchaField(widget=ReCaptchaV2Checkbox(attrs={
        'data-theme': 'white',
        'data-size': 'normal',
    }))

    class Meta:
        model = CustomUser
        fields = ['username', 'email', 'company']

    def signup(self, request, user):
        user.company = self.cleaned_data['company']
        user.timezone = 'GMT'
        user.save()


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        fields = ['username', 'email', 'company', 'timezone']

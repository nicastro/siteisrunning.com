from django.conf.urls import url
from django.urls import include, path

from .views import update_account, delete_account

urlpatterns = [
    url(r"^update/$", update_account, name='update_account'),
    url(r"^delete/$", delete_account, name='delete_account')
]

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model
from users.forms import CustomUserChangeForm

User = get_user_model()


@login_required
def update_account(request):
    form = CustomUserChangeForm(request.POST or None, instance=request.user)
    if form.is_valid():
        user = request.user
        user.save()
        messages.success(request, 'Account updated successfully')
        return redirect('update_account')

    return render(request, 'update_account_form.html', {'form': form})


@login_required
def delete_account(request):
    if request.method == 'POST':
        user = request.user
        user.is_active = False
        user.save()
        user.delete()
        messages.success(request, 'Account deleted successfully')
        return redirect('delete_account')

    return render(request, 'delete_account_form.html')

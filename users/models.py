# users/models.py
import pytz
from allauth.account.models import EmailAddress
from django.contrib.auth.models import AbstractUser
from django.db import models

from django.apps import apps


class CustomUser(AbstractUser):
    # add additional fields in here
    company = models.TextField(blank=True)
    timezone = models.CharField(max_length=40, null=True, blank=True, choices=[(n, n) for n in pytz.all_timezones])

    def __str__(self):
        return self.email

    def is_paying(self):
        payment_model = apps.get_model('payment', 'Payment')
        if payment_model.objects.filter(owner_id=self.id).exists():
            payment = payment_model.objects.get(owner_id=self.id)
            if not payment.is_user_paying_expired():
                return True

        return False

    def payment_attributes_for_render(self):
        payment_model = apps.get_model('payment', 'Payment')
        is_user_paying = False
        payment_id = None
        payment_expires_at = None
        expires_reminder_sent_at = None

        if payment_model.is_user_paying(self):
            payment = payment_model.objects.get(owner_id=self.id)
            if not payment.is_user_paying_expired():
                is_user_paying = True
                payment_id = payment.id
                payment_expires_at = payment.payment_expires_at.strftime("%Y/%m/%d")
                expires_reminder_sent_at = payment.expires_reminder_sent_at.strftime("%Y/%m/%d")

        return {
            "is_user_paying": is_user_paying,
            "payment_id": payment_id,
            "payment_expires_at": payment_expires_at,
            "expires_reminder_sent_at": expires_reminder_sent_at,
        }

from django.contrib.auth import get_user_model
from django.utils import timezone
#
import pytz
#
#
# class TimezoneMiddleware(object):
#
#     def process_request(self, request):
#         if request.user.is_authenticated():
#             timezone.activate(pytz.timezone(request.user.CustomUser))
#         else:
#             timezone.deactivate()
#
#     def __init__(self, get_response):
#         self.get_response = get_response
#
#     def __call__(self, request):
#         return self.get_response(request)
from django.utils.deprecation import MiddlewareMixin


class GetGuitarists(MiddlewareMixin):

    def process_request(self, request):
        if request.user.is_authenticated:
            if request.user.timezone:
                timezone.activate(pytz.timezone(request.user.timezone))
            else:
                timezone.activate('UTC')
        else:
            timezone.deactivate()

from django.contrib import messages
from django.core.mail import send_mail, BadHeaderError, EmailMessage
from django.shortcuts import render, redirect

# Create your views here.
from siteisrunning.forms import ContactForm
from siteisrunning.settings import BASIC_PLAN_MIN_PERIOD_CHECK_MINUTES, BASIC_PLAN_MIN_WHEN_OFFLINE_SEND_NEXT_MINUTES, \
    BASIC_PLAN_MAX_EMAIL_ADDRESSES, BASIC_PLAN_MAX_ACTIVE_CHECKS, ADMIN_EMAIL, DEFAULT_FROM_EMAIL


def redirect_view(request):
    response = redirect('home')
    return response


# @cache_page(90000)
def term_condition_privacy(request):
    return render(request, 'base/term_condition_privacy.html', {})


# @cache_page(90000)
def what(request):
    return render(request, 'base/what.html',
                  {'BASIC_PLAN_MAX_ACTIVE_CHECKS': BASIC_PLAN_MAX_ACTIVE_CHECKS,
                   'BASIC_PLAN_MIN_PERIOD_CHECK_MINUTES': BASIC_PLAN_MIN_PERIOD_CHECK_MINUTES,
                   'BASIC_PLAN_MIN_WHEN_OFFLINE_SEND_NEXT_MINUTES': BASIC_PLAN_MIN_WHEN_OFFLINE_SEND_NEXT_MINUTES,
                   'BASIC_PLAN_MAX_EMAIL_ADDRESSES': BASIC_PLAN_MAX_EMAIL_ADDRESSES})


def contact(request):
    if request.method == 'GET':
        form = ContactForm()
    else:
        form = ContactForm(request.POST or None)
        if form.is_valid():
            from_email = form.cleaned_data['from_email']
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message']
            try:
                mail_admin_headers = {'Reply-To': from_email}
                mail_admin = EmailMessage(subject, message, ADMIN_EMAIL, [ADMIN_EMAIL], headers=mail_admin_headers)

                mail_user_copy = EmailMessage(subject, message, ADMIN_EMAIL, [from_email])

                mail_admin.content_subtype = mail_user_copy.content_subtype = "html"
                mail_admin.send()
                mail_user_copy.send()

            except BadHeaderError:
                messages.error(request, 'There was a problem. Contact us here: admin@siteisrunning.com')
                return render(request, "base/contact.html", {'form': form})
            except TimeoutError:
                messages.error(request, 'There was a problem. Contact us here: admin@siteisrunning.com')
                return render(request, "base/contact.html", {'form': form})

            return render(request, 'base/contact_success.html')

    return render(request, "base/contact.html", {'form': form})


# @cache_page(90000)
def home(request):
    return render(request, 'base/home.html',
                  {'BASIC_PLAN_MAX_ACTIVE_CHECKS': BASIC_PLAN_MAX_ACTIVE_CHECKS,
                   'BASIC_PLAN_MIN_PERIOD_CHECK_MINUTES': BASIC_PLAN_MIN_PERIOD_CHECK_MINUTES,
                   'BASIC_PLAN_MIN_WHEN_OFFLINE_SEND_NEXT_MINUTES': BASIC_PLAN_MIN_WHEN_OFFLINE_SEND_NEXT_MINUTES,
                   'BASIC_PLAN_MAX_EMAIL_ADDRESSES': BASIC_PLAN_MAX_EMAIL_ADDRESSES})

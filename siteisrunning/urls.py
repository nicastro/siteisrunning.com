"""siteisrunning URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView

from siteisrunning.views import redirect_view, term_condition_privacy, contact, home, what

urlpatterns = [
    path('admin/', admin.site.urls),
    path('redirect/', redirect_view),

    # path('accounts/', include('accounts.urls')),
    # path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/', include('allauth.urls')),
    path('accounts/', include('users.urls')),
    path('accounts/', include('payment.urls')),

    path('', home, name='home'),
    path('websites/', include('check.urls')),
    path('terms-condition-privacy/', term_condition_privacy, name='term_condition_privacy'),
    path('contact/', contact, name='contact'),
    path('what/', what, name='what'),

    path('robots.txt/', TemplateView.as_view(template_name='base/robots.txt', content_type='text/plain'),
         name='robots'),

]
